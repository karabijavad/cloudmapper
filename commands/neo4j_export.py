import argparse
import logging
import os
from urllib.parse import urlparse

from shared.common import parse_arguments, query_aws
from shared.nodes import Account, Region

from neo4j.v1 import GraphDatabase

__description__ = "Export data to neo4j"


def run(arguments):
    parser = argparse.ArgumentParser()
    parser.add_argument("--neo4j-url")
    args, accounts_data, config = parse_arguments(arguments, parser)
    accounts = [Account(None, a) for a in accounts_data]

    neo4j_url = args.neo4j_url or os.environ.get("NEO4J_URL")
    parsed_url = urlparse(neo4j_url)
    neo4j_username = parsed_url.username
    neo4j_password = parsed_url.password
    driver = GraphDatabase.driver(neo4j_url, auth=(neo4j_username, neo4j_password))
    session = driver.session()
    session.run("MATCH (n) DETACH DELETE n")
    session.run("CREATE CONSTRAINT ON (a:Account) ASSERT a.arn IS UNIQUE")
    session.run("CREATE CONSTRAINT ON (r:Region) ASSERT r.arn IS UNIQUE")
    session.run("CREATE CONSTRAINT ON (v:VPC) ASSERT v.id IS UNIQUE")
    session.run("CREATE CONSTRAINT ON (i:Instance) ASSERT i.id IS UNIQUE")

    for account in accounts:
        describe_regions = query_aws(account, "describe-regions")
        regions = [Region(account, r) for r in describe_regions['Regions']]

        for region in regions:
            describe_vpcs = query_aws(account, "ec2-describe-vpcs", region)
            for vpc_data in describe_vpcs.get('Vpcs', []):
                session.run("""
                    MERGE (a:Account { id: $account.id })
                    MERGE (r:Region { id: $region.RegionName})
                    MERGE (v:VPC { id: $vpc_data.VpcId })
                    MERGE (a)-[:vpc]->(v)
                    MERGE (r)-[:vpc]->(v)
                """, {
                    'account': account.json,
                    'region': region.json,
                    'vpc_data': vpc_data,
                })

            describe_sgs = query_aws(account, "ec2-describe-security-groups", region)
            for sg_data in describe_sgs.get('SecurityGroups', []):
                if not sg_data.get('VpcId'):
                    logging.warning("security group without vpc id %s", sg_data)
                    session.run("""
                        MERGE (sg:SecurityGroup { id: $sg_data.GroupId })
                    """, { 'sg_data': sg_data })
                else:
                    session.run("""
                        MERGE (v:VPC { id: $sg_data.VpcId })
                        MERGE (sg:SecurityGroup { id: $sg_data.GroupId })
                        MERGE (v)-[:security_group]->(sg)
                    """, { 'sg_data': sg_data })

            describe_instances = query_aws(account, "ec2-describe-instances", region)
            for reservation_data in describe_instances.get('Reservations', []):
                for instance_data in reservation_data.get('Reservations', []):
                    session.run("""
                        MERGE (v:VPC { id: instance_data.VpcId })
                        MERGE (i:Instance { id: instance_data.InstanceId })
                        MERGE (v)-[:instance]->(i)
                    """, { 'instance_data': instance_data })
                    for instance_sg_data in instance_data['SecurityGroups']:
                        session.run("""
                            MERGE (i:Instance { id: instance_data.InstanceId })
                            MERGE (s:SecurityGroup { id: instange_sg_data.GroupId })
                            MERGE (i)-[:security_group]->(s)
                        """, {
                            'instance_data': instance_data,
                            'instance_sg_data': instance_sg_data,
                        })

            describe_db_instances = query_aws(account, "rds-describe-db-instances", region)
            describe_load_balancers = query_aws(account, "elb-describe-load-balancers", region)

            session.run("""
                FOREACH (db_instances_data in $describe_db_instances.DBInstances |
                    MERGE (db:DB { arn: db_instances_data.DBInstanceArn } )
                    MERGE (a)-[:db_instance]->(db)
                    MERGE (r)-[:db_instance]->(db)
                    FOREACH (vs IN db_instances_data.VpcSecurityGroups |
                        MERGE (v)-[:security_group]->(s:SecurityGroup { id: vs.VpcSecurityGroupId })
                        MERGE (db)-[:security_group]->(s)
                    )
                )
                FOREACH (lb_data IN $describe_load_balancers.LoadBalancerDescriptions |
                    MERGE (lb:LoadBalancer { name: lb_data.LoadBalancerName })
                    MERGE (vpc:VPC {id: lb_data.VPCId })
                    MERGE (vpc)-[:load_balancer]->(lb)

                    FOREACH (lb_instance_data IN lb_data.Instances |
                        MERGE (i:Instance { id: lb_instance_data.InstanceId })
                        MERGE (lb)-[:instance]->(i)
                    )
                    FOREACH (lb_sg_id IN lb_data.SecurityGroups |
                        MERGE (v)-[:security_group]->(s:SecurityGroup { id: lb_sg_id })
                        MERGE (lb)-[:security_group]->(s)
                    )
                )
            """, **{
                'describe_instances': describe_instances,
                'describe_db_instances': describe_db_instances,
                'describe_load_balancers': describe_load_balancers,
            })

    session.close()
