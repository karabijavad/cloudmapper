FROM ubuntu:18.04

# prevent apt-get install from prompting during build
ENV DEBIAN_FRONTEND=noninteractive
# use UTF-8 by default
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# install system packages
RUN apt-get update \
  && apt-get install -y \
    python3.7 python3.7-dev python3-pip \
    awscli git jq \
    build-essential autoconf libtool \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* && \
  pip3 install pipenv

# create the cloudmapper user
RUN useradd -ms /bin/bash cloudmapper
USER cloudmapper
WORKDIR /home/cloudmapper

# shallow clone the repo, then delete .git since we dont need it
RUN git clone --depth=1 --branch javad https://gitlab.com/karabijavad/cloudmapper.git /home/cloudmapper/cloudmapper
WORKDIR /home/cloudmapper/cloudmapper
RUN rm -rf .git

# install dependencies and create venv in project dir
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --skip-lock --dev
# set VIRTUAL_ENV and PATH, so that the venv
# is automatically active when entering the container
ENV VIRTUAL_ENV="/home/cloudmapper/cloudmapper/.venv"
ENV PATH="/home/cloudmapper/cloudmapper/.venv/bin:${PATH}"

VOLUME /home/cloudmapper/cloudmapper/config.json
VOLUME /home/cloudmapper/cloudmapper/account-data
VOLUME /home/cloudmapper/cloudmapper/web/data.json

EXPOSE 8000

CMD ["./cloudmapper.py", "webserver", "--public"]
